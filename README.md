# Star Trek Voyager: Elite Force map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Star Trek Voyager: Elite Force game.

This gamepack is based on STVEF game pack modified by LithTechGuru:

- https://www.moddb.com/games/star-trek-voyager-elite-force/downloads/netradiant-custom-config

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/STVEFPack/ in the future.
